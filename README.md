# XSpotify

### A modified Spotify client for Windows
![Screenshot](https://i.imgur.com/iJ0z7vn.png)

[![HitCount](http://hits.dwyl.io/meik97/XSpotify.svg)](https://github.com/meik97/XSpotify)
[![Downloads](https://img.shields.io/github/downloads/meik97/XSpotify/total.svg?color=green)](https://github.com/meik97/XSpotify/releases)
[![Discord](https://discordapp.com/api/guilds/671076782467973130/widget.png)](http://discord.gg/EByRp27)


## Features:

- DRM bypass: Download all songs directly from Spotify servers --> https://www.youtube.com/watch?v=E8a-dB1HIQU
- Enabled the skip button for ads blocking (includes audio and video ads)
- Auto skipping function for audio based ads
- Quality and format: 160 kb/s, 32-bit, 44100 Hz .ogg


## Requirements:

You need Microsoft's [Visual C++ Redistributable Runtimes](https://github.com/abbodi1406/vcredist) installed.

## How to use:

- [Download XSpotify](https://github.com/meik97/XSpotify/releases)
- Place all files into your Spotify profile, it's located under `%appdata%\Spotify`.
- Manually start XSpotify

After you have started XSpotify, the console window will appear. 
  
  
![Command Line Interface](https://i.imgur.com/uRwqF2L.png)
  
## Tested versions:

- 1.1.24.91.g4ca6d5eb (15.01.2020)

## Known bugs:

- Crash after song skipping

