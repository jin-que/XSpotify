#include <iostream>
#include <urlmon.h>
#include <fstream>
#include <string>
#include <WinInet.h>
#pragma comment(lib,"wininet.lib")
#pragma comment(lib,"ws2_32.lib")

using namespace std;

std::string HttpRequest(std::string site, std::string param)
{
	std::string authHeader;
	HINTERNET hInternet = InternetOpenW(L"1337", INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, 0);
	
	authHeader = "Accept: application/json\nAccept-Language: de\nApp-Platform: WebPlayer\nUser-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36\nSpotify-App-Version: 1576851415\nReferer: https://open.spotify.com/\nCookie: '. ' sp_t=8216320f1a1b55329cc788d3861bda2d; sp_adid=e0c4b4b0-c2ce-47b1-91cd-9a6e500bd236; _gcl_au=1.1.930311039.1568296866; _hjid=3cfe7894-03cb-49b7-a008-59e9e02a58c6; _fbp=fb.1.1568296866726.817501151; open_env=php; sp_ab=%7B%222019_04_premium_menu%22%3A%22control%22%7D; sp_gaid=0088fcae70df1230ce6227ad4c27d9dfc16b52d53070edbb54dadd; sp_phash=5b159869266f788cd98cb9ddbdfb3bcbeacac1cd; spot=%7B%22t%22%3A1576582774%2C%22m%22%3A%22jp%22%2C%22p%22%3A%22open%22%7D; optimizelyEndUserId=oeu1576847786549r0.3765501627206682; optimizelySegments=%7B%226174980032%22%3A%22search%22%2C%226176630028%22%3A%22none%22%2C%226179250069%22%3A%22false%22%2C%226161020302%22%3A%22gc%22%7D; optimizelyBuckets=%7B%7D; sp_last_utm=%7B%22utm_campaign%22%3A%22your_account%22%2C%22utm_medium%22%3A%22menu%22%2C%22utm_source%22%3A%22spotify%22%7D; __gads=ID=46eef32200ade146:T=1577188825:S=ALNI_MaSsNN-fkmKLUqUmpi87Lkk6BJz4Q; _ga_0KW7E1R008=GS1.1.1577195047.3.1.1577195874.0; _derived_epik=dj0yJnU9ZHhLV1NmV3hhb2VKOWhEcWdGUTRuN0RhbC1OdEFBMnMmbj1tOU5DS1E5UU11U1pQeWlLNTJMRW1RJm09NyZ0PUFBQUFBRjRDR1dJ; _ga=GA1.2.1103680000.1568296854; sp_landing=http%3A%2F%2Fopen.spotify.com%2F; sss=1; _gid=GA1.2.1895535078.1577587497; sp_dc=AQAMQHYD7SGCY6Hu8aFyjDsPfTkv7s4LqxCm9fOMMsph1GR8yCxW_9cqOoeQH8TeFvcTrTEfcFcCiEZemGSWpCcSE9ESBTJVoW0X6ICQUg; sp_key=99d88c42-2b0f-41c8-b3e3-e764398ce474; _gat_gtag_UA_5784146_31=1\n";
	
	if (hInternet == NULL)
	{
		return "InternetOpenW failed(hInternet): " + GetLastError();
	}
	else
	{
		HINTERNET hConnect = InternetConnectA(hInternet, site.c_str(), 80, NULL, NULL, INTERNET_SERVICE_HTTP, 0, NULL);

		if (hConnect == NULL)
		{
			return "InternetConnectW failed(hConnect == NULL): " + GetLastError();
		}
		else
		{
			HINTERNET hRequest = HttpOpenRequestA(hConnect, "GET", param.c_str(), NULL, NULL, NULL, INTERNET_FLAG_NO_COOKIES, 0);

			if (hRequest == NULL)
			{
				return "HttpOpenRequestW failed(hRequest == NULL): " + GetLastError();
			}
			else
			{
			    HttpAddRequestHeadersA(hRequest, authHeader.c_str(), -1, HTTP_ADDREQ_FLAG_ADD | HTTP_ADDREQ_FLAG_REPLACE);

				BOOL bRequestSent = HttpSendRequestA(hRequest, NULL, 0, NULL, 0);

				if (!bRequestSent)
				{
					return "!bRequestSent    HttpSendRequestW failed with error code " + GetLastError();
				}
				else
				{
					std::string strResponse;
					const int nBuffSize = 1024;
					char buff[nBuffSize];

					BOOL bKeepReading = true;
					DWORD dwBytesRead = -1;

					while (bKeepReading && dwBytesRead != 0)
					{
						bKeepReading = InternetReadFile(hRequest, buff, nBuffSize, &dwBytesRead);
						strResponse.append(buff, dwBytesRead);
					}
					return strResponse;
				}

				InternetCloseHandle(hRequest);
			}

			InternetCloseHandle(hConnect);
		}

		InternetCloseHandle(hInternet);
	}
}

int main()
{
	std::string response = HttpRequest("open.spotify.com", "access_token?reason=transport&productType=web_player");
	std::string finalHeader = strtok((char*)(response.substr(response.find("accessToken") + 14)).c_str(), "\"");
	std::ofstream myfile;
	myfile.open("token.txt");
	myfile << finalHeader;
	myfile.close();
}


