// ==========================================================
// Project: XSpotify
// 
// Component: XSpotify.dll
//
// Purpose: Downloading and decrypting Spotify songs directly
//          from Spotify servers
//
// Initial author: Meik Wochnik
//
// Started: 28.12.2019
//
// *thanks to BAKADESU for helping
// ==========================================================

#pragma once
#include "..\\include\BaseInclude.hpp"
#include <openssl/evp.h>
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "libsslMT.lib")
#pragma comment(lib, "Crypt32.lib")
#pragma comment(lib, "libcryptoMT.lib")

struct DecryptedSong_s
{   
	int decryptedtext_len;
	unsigned char rawDecryptedData[50000000];
	unsigned char outputData[50000000];
	unsigned char songName_json[5000];
	std::string currentSong;
	std::string songName;
	std::string rawSongData;
} DecryptedSong;

struct EncryptedSong_s
{
	unsigned char encryptedData[50000000]; //cipher
	const char* fileID;
	std::string rawSongData;
	std::string rawHostData;
	std::string parsedHost;
	std::string parsedParam;
	std::string decryptionKey;
} EncryptedSong;

std::string HttpRequest(std::string site, std::string param, std::string accessToken, bool isaccessToken)
{
	std::string authHeader;
	HINTERNET hInternet = InternetOpenW(L"1337", INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, 0);

	if (isaccessToken == true)
	{
		authHeader = "Authorization: Bearer " + accessToken;
	}
	else
	{
		authHeader = "1337";
	}

	if (hInternet == NULL)
	{
		return "InternetOpenW failed(hInternet): " + GetLastError();
	}
	else
	{
		HINTERNET hConnect = InternetConnectA(hInternet, site.c_str(), 80, NULL, NULL, INTERNET_SERVICE_HTTP, 0, NULL);

		if (hConnect == NULL)
		{
			return "InternetConnectW failed(hConnect == NULL): " + GetLastError();
		}
		else
		{
			HINTERNET hRequest = HttpOpenRequestA(hConnect, "GET", param.c_str(), NULL, NULL, NULL, INTERNET_FLAG_NO_AUTH, 0);

			if (hRequest == NULL)
			{
				return "HttpOpenRequestW failed(hRequest == NULL): " + GetLastError();
			}
			else
			{
				HttpAddRequestHeadersA(hRequest, authHeader.c_str(), -1, HTTP_ADDREQ_FLAG_ADD | HTTP_ADDREQ_FLAG_REPLACE);

				BOOL bRequestSent = HttpSendRequestA(hRequest, NULL, 0, NULL, 0);

				if (!bRequestSent)
				{
					return "!bRequestSent    HttpSendRequestW failed with error code " + GetLastError();
				}
				else
				{
					std::string strResponse;
					const int nBuffSize = 1024;
					char buff[nBuffSize];

					BOOL bKeepReading = true;
					DWORD dwBytesRead = -1;

					while (bKeepReading && dwBytesRead != 0)
					{
						bKeepReading = InternetReadFile(hRequest, buff, nBuffSize, &dwBytesRead);
						strResponse.append(buff, dwBytesRead);
					}
					return strResponse;
				}

				InternetCloseHandle(hRequest);
			}

			InternetCloseHandle(hConnect);
		}

		InternetCloseHandle(hInternet);
	}
}

void handleErrors(void)
{
	abort();
}

int decrypt(unsigned char* ciphertext, int ciphertext_len, unsigned char* key, unsigned char* iv, unsigned char* plaintext)
{
	EVP_CIPHER_CTX* ctx;

	int len;

	int plaintext_len;

	if (!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_128_ctr(), NULL, key, iv))
		handleErrors();


	if (1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
		handleErrors();
	plaintext_len = len;


	if (1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len))
		handleErrors();
	plaintext_len += len;

	EVP_CIPHER_CTX_free(ctx);

	return plaintext_len;
}

std::string hex2string(std::string str)
{
	std::string res;
	res.reserve(str.size() / 2);
	for (int i = 0; i < str.size(); i += 2)
	{
		std::istringstream iss(str.substr(i, 2));
		int temp;
		iss >> std::hex >> temp;
		res += static_cast<char>(temp);
	}

	return res;
}

void __declspec(naked) AES_set_encrypt_key_stub(unsigned int* key, DWORD* userKey, int bits)
{
	__asm
	{
		push     ebp
		mov      ebp, esp
		mov      edx, [ebp + 8]
		push     105B8F6h
		retn
	}
}

void AES_set_encrypt_key_hk(unsigned int* key, DWORD* userKey, int bits)
{
	unsigned char decrypt_key[16] = { 0 };
	char decrypt_key_str[33] = { 0 };

	if (bits == 128) 
	{
		unsigned char _userKey[16] = { 0 };

		for (int i = 0; i < 16; i++) {
			_userKey[i] = *(char*)((DWORD)userKey + i);
		}

		if (memcmp(_userKey, decrypt_key, 16) != 0) 
		{
			for (int i = 0; i < 16; i++) 
			{
				sprintf(decrypt_key_str + i * 2, "%02X", _userKey[i]);
			}

			EncryptedSong.decryptionKey = decrypt_key_str;
		}
	}

	AES_set_encrypt_key_stub(key, userKey, bits);
}

void removeForbiddenChar(std::string* s)
{
	std::string::iterator it;

	for (it = s->begin(); it < s->end(); ++it) {
		switch (*it)
		{
		case '/':case '\\':case ':':case '?':case '"':case '<':case '>':case '|': case ';':case '+':case '@':case '=':case ',':case '#': case '*':case '~':
			*it = '_';
		}
	}
}

void PressMediaKey(int key)
{
	INPUT input;
	WORD vkey = key;
	input.type = INPUT_KEYBOARD;
	input.ki.wScan = MapVirtualKey(vkey, MAPVK_VK_TO_VSC);
	input.ki.time = 0;
	input.ki.dwExtraInfo = 0;
	input.ki.wVk = vkey;
	input.ki.dwFlags = 0;
	SendInput(1, &input, sizeof(INPUT));
	input.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &input, sizeof(INPUT));
}

std::string GetAccessToken()
{
	/*
	As we are not able to request multiple tokens at runtime,
	we need to request them from another process.
	*/
	ShellExecute(NULL, L"open", L".\\token.exe", NULL, NULL, SW_HIDE);
	Sleep(1000);

	std::ifstream ifs("token.txt");
	std::string accessToken((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	return accessToken;
}

std::string GetRawSongData()
{
	std::string SongID = EncryptedSong.fileID;
	EncryptedSong.rawHostData = HttpRequest("spclient.wg.spotify.com", "/storage-resolve/files/audio/interactive_prefetch/" + SongID + "?product=0", GetAccessToken(), true);
	EncryptedSong.parsedHost = (EncryptedSong.rawHostData.substr(EncryptedSong.rawHostData.find("https://") + 8)).erase(EncryptedSong.rawHostData.substr(EncryptedSong.rawHostData.find("https://") + 8).find("/audio/"));
	EncryptedSong.parsedParam = EncryptedSong.rawHostData.substr(EncryptedSong.rawHostData.find("/audio/")).erase(EncryptedSong.rawHostData.substr(EncryptedSong.rawHostData.find("/audio/")).find("="));
	EncryptedSong.rawSongData = HttpRequest(EncryptedSong.parsedHost, EncryptedSong.parsedParam, "", false);
	return EncryptedSong.rawSongData;
}

std::string DeleteOGGHeader(std::string rawSongData)
{
	return rawSongData.substr(rawSongData.find("����OggS") + 4);
}

bool DownloadFileProcess()
{
	std::string IVKEY = hex2string("72E067FBDDCBCF77EBE8BC643F630D93");
	std::string KEY = hex2string(EncryptedSong.decryptionKey);

	memcpy(EncryptedSong.encryptedData, GetRawSongData().data(), GetRawSongData().size());
	DecryptedSong.decryptedtext_len = decrypt(EncryptedSong.encryptedData, GetRawSongData().length(), (unsigned char*)KEY.c_str(), (unsigned char*)IVKEY.c_str(), DecryptedSong.rawDecryptedData);
	DecryptedSong.rawSongData.assign((char*)& DecryptedSong.rawDecryptedData[0], sizeof(DecryptedSong.rawDecryptedData));
	memcpy(DecryptedSong.outputData, DeleteOGGHeader(DecryptedSong.rawSongData).data(), DeleteOGGHeader(DecryptedSong.rawSongData).size());

	if(DeleteOGGHeader(DecryptedSong.rawSongData).find("OggS") != 0)
	{
		//hotfix to restart the decryption with the right key
		PressMediaKey(0xB2);
		Sleep(5000);
	}
	
	if (!DecryptedSong.currentSong.empty())
	{
			std::string id3songname_raw = HttpRequest("api.spotify.com", "/v1/tracks/" + DecryptedSong.currentSong.substr(DecryptedSong.currentSong.find("spotify:track:") + 14), GetAccessToken(), true);
			DecryptedSong.songName = strtok((char*)(id3songname_raw.substr(id3songname_raw.find("is_local") + 31)).c_str(), "\"");
			std::cout << DecryptedSong.songName << std::endl;
	}

    removeForbiddenChar(&DecryptedSong.songName);
	std::ofstream decryptedsongoutput(".\\XSongs\\" + DecryptedSong.songName + ".ogg", std::ios_base::binary);
	decryptedsongoutput.write((char*)DecryptedSong.outputData, DecryptedSong.decryptedtext_len);
	decryptedsongoutput.close();
} 

void __declspec(naked) __fastcall Signal_stub(void* _this, DWORD edx, int a2, int a3)
{
	__asm
	{
		push    ebp
		mov     ebp, esp
		push    -1
		push    1169310h
		push    0B3A7CAh
		retn
	}
}

void __fastcall Signal_hk(void* _this, DWORD edx, int a2, int a3)
{
	Sleep(1000);
	DownloadFileProcess();
	Signal_stub(_this, edx, a2, a3);
}

void __declspec(naked) CmdAddText_stub2(int a1, int a2, const char* fmt, const char* dummy0, int dummy1, int dummy2, int dummy3, int dummy4, int dummy5)
{
	__asm
	{
		push    ebp
		mov     ebp, esp
		push    esi
		mov     esi, [ebp + 8]
		lea     eax, [ebp + 20]
		push    107BAEAh
		retn
	}
}

void CmdAddText_hk2(int a1, int a2, const char* fmt, const char* dummy0, int dummy1, int dummy2, int dummy3, int dummy4, int dummy5)
{
	if (fmt[8] == char(116) && fmt[9] == char(114) && fmt[10] == char(97) && fmt[11] == char(99) && fmt[12] == char(107) && fmt[13] == char(95) && fmt[14] == char(117) && fmt[15] == char(114) && fmt[16] == char(105))
	{
		if (dummy0[8] == char(97) && dummy0[9] == char(100))
		{
			HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
			std::cout << "[Ad detected]: " << dummy0 << std::endl;
		}
		else if (dummy0[8] == char(116) && dummy0[9] == char(114))
		{
			DecryptedSong.currentSong = dummy0;
			std::cout << DecryptedSong.currentSong << std::endl;
		}
	}

	CmdAddText_stub2(a1, a2, fmt, dummy0, dummy1, dummy2, dummy3, dummy4, dummy5);
}

void __declspec(naked) GetFileID_stub(int* a1, int a2)
{
	__asm
	{
		push    ebp
		mov     ebp, esp
		mov     ecx, [ebp + 8]
		push    dword ptr[ebp + 12]
		push    76E649h
		retn
	}
}

void GetFileID_hk(int* a1, int a2)
{
	GetFileID_stub(a1, a2);
	EncryptedSong.fileID = (char*)(*(DWORD*)((*(DWORD*)(a1)) + 40));
	Sleep(1000);
	GetFileID_stub(a1, a2);
}

void XPatchDownloads()
{
	Hook::InstallJmp(Signal, Signal_hk);
	Hook::InstallJmp(CmdAddText, CmdAddText_hk2);
	Hook::InstallJmp(aes_set_encrypt_key, AES_set_encrypt_key_hk);
	Hook::InstallJmp(GetFileID, GetFileID_hk);
}