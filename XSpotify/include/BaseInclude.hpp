#pragma once

//Base
#include <Windows.h>
#include <iostream>
#include <urlmon.h>
#include <tchar.h>
#include <string>
#include <fstream>
#include <WinInet.h>
#include <shellapi.h>
#include <sstream>
#include <thread>

//Libs
#pragma comment(lib, "wininet.lib")

//Hooking
#include "Hooking.hpp"

//Functions
#include "..\\Modules\Functions.hpp"

//Patches
#include "..\\Modules\XPatchAds.hpp"
#include "..\\Modules\XPatchBitrate.hpp"
#include "..\\Modules\XPatchDownloads.hpp"



