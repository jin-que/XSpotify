﻿// ==========================================================
// Project: XSpotify
// 
// Component: XSpotify.dll
//
// Purpose: Entry point
//          
// Initial author: Meik Wochnik
//
// Started: 14.10.2019
// ==========================================================

#include "pch.h"
#include "include/BaseInclude.hpp"

void PatchSpotify()
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, 10);
	std::cout << R"(
	                            __   __ _____             _   _  __       
	                            \ \ / // ____|           | | (_)/ _|      
	                             \ V /| (___  _ __   ___ | |_ _| |_ _   _ 
	                              > <  \___ \| '_ \ / _ \| __| |  _| | | |
	                             / . \ ____) | |_) | (_) | |_| | | | |_| |
	                            /_/ \_\_____/| .__/ \___/ \__|_|_|  \__, |
	                                         | |                     __/ |
	                                         |_|                    |___/ 
																								)" << std::endl;

    XPatchAds();
	XPatchDownloads();
	//XPatchBitrate();
}

DWORD WINAPI Exec(LPVOID lpParam)
{
	AllocConsole();
	freopen("CONOUT$", "w", stdout);
	PatchSpotify();
	return 0;
}

BOOL WINAPI DllMain(HINSTANCE hModule, DWORD dwAttached, LPVOID lpvReserved)
{
	if (dwAttached == DLL_PROCESS_ATTACH)
	{
		CreateThread(NULL, 0, &Exec, NULL, 0, NULL);
	}
	return 1;
}